FROM rust:latest
ENV RUST_BACKTRACE=full \
    RUSTUP_HOME=/usr/local/rustup \
    CARGO_HOME=/usr/local/cargo \
    PATH=$CARGO_HOME/bin:$PATH
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y musl-tools libssl-dev findutils
RUN rustup toolchain install stable beta nightly -t x86_64-unknown-linux-gnu -t x86_64-unknown-linux-musl -c clippy -c rustfmt && \
    rustup default stable
RUN cargo install \
      sccache
ENV RUSTC_WRAPPER=$CARGO_HOME/bin/sccache
RUN cargo install \
      cargo-binstall \
      cargo-quickinstall \
      cargo-audit \
      cargo-binutils \
      cargo-bloat \
      cargo-cache \
      cargo-deb \
      cargo-geiger \
      cargo-outdated \
      cargo-release \
      cargo-tarpaulin \
      git-cliff \
      gitlab-report

