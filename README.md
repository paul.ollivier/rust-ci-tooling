# GitLab-CI rust tooling

This project is here to help me build rust projects on GitLabCI.

It's mainly a file available for inclusion that contains useful, reusable bits & blobs of jobs.

## Usage

```yaml
# .gitlab-ci.yml
include: https://gitlab.com/paul.ollivier/rust-ci-tooling/-/raw/main/rust.gitlab-ci.yml
```

That's it for the basics!

Here's a handy diagram of what's included:

```mermaid
graph RL
  check:clippy & check:geiger & test:test & test:bench & build:matrix & release:build:bin -. uses .-> cargo_opts
  linkStyle 0 stroke:#ff3,stroke-width:4px;
  linkStyle 1 stroke:#ff3,stroke-width:4px;
  linkStyle 2 stroke:#ff3,stroke-width:4px;
  linkStyle 3 stroke:#ff3,stroke-width:4px;
  linkStyle 4 stroke:#ff3,stroke-width:4px;
  linkStyle 5 stroke:#ff3,stroke-width:4px;
  subgraph "common_config"
    matrix_channels[*default-matrix-channels]
    matrix_targets[*default-matrix-targets]
    cargo_opts[$CARGO_OPTS]
    deploy_pages[$DEPLOY_PAGES]
    deploy_gitlab[$DEPLOY_GITLAB]
    deploy_crates[$DEPLOY_CRATES]
    rust_default([.rust-default])
    do_audit[$DO_AUDIT]
    require_audit[$REQUIRE_AUDIT]
    require_clippy[$REQUIRE_CLIPPY]
    require_fmt[$REQUIRE_FMT]
  end
  subgraph "check"
    check:clippy([check:clippy]) -. extends .-> rust_default
    check:clippy -. is conditioned by .-> require_clippy
    check:fmt([check:fmt]) -. extends .-> rust_default
    check:fmt -. is conditioned by .-> require_fmt
    check:audit([check:audit]) -. extends .-> rust_default
    check:geiger([check:geiger]) -. extends .-> rust_default
    check:audit & check:geiger -. is conditioned by .-> do_audit & require_audit
  end
  subgraph "build"
    build:matrix([build:matrix]) -. references .-> matrix_channels & matrix_targets
    build:matrix -- needs--> check:clippy
    build:matrix -. extends .-> rust_default
    build:docs([build:docs]) -- needs --> check:clippy & check:fmt
    build:docs -. extends .-> rust_default
  end
  subgraph "test"
    test:test([test:test]) == depends ==> build:matrix
    test:test -. references .-> matrix_channels
    test:test -. extends .-> rust_default
    test:bench([.test:bench]) == depends ==>build:matrix
    test:bench -. extends .-> rust_default
  end
  subgraph "release"
    release:build:bin([release:build:bin]) -- needs --> test:test
    release:build:bin -. extends .-> rust_default
    release:pages([.release:pages]) -- needs --> test:test
    release:pages == depends ==> build:docs
    release:pages -. references .-> deploy_pages
    release:gitlab([release:gitlab]) == depends ==> release:build:bin
    release:gitlab -. references .-> deploy_gitlab
    release:crates([release:crates]) -- needs --> test:test
    release:crates -. references .-> deploy_crates
    pages([pages]) -. "extends/aliases"  .-> release:pages
  end
```

## What's included
All steps for a classic rust build:

### Check
- [cargo clippy]
- [rustfmt]
- [cargo-audit]
- [cargo-geiger]

### build
The build is done via [matrix parallelization](https://docs.gitlab.com/ee/ci/yaml/#parallelmatrix), with the following default definition:

```yaml
# .gitlab-ci.yml
build:matrix:
  extends: .rust-default
  stage: build
  needs: [ "check:clippy" ]
  parallel:
    matrix:
      - CHANNEL: *matrix-channels
        TARGET: *matrix-targets
        PROFILE: [debug]
```
You can override this like so, if you don't want to use the defaults:
```yaml
# .gitlab-ci.yml
.my-channels: &matrix-channels
  - stable
  - beta
.my-targets: &matrix-targets
  - x86_64-unknown-linux-gnu # for classic© computers®
  - x86_64-unknown-linux-musl # for alpine docker & other systems where it's desirable to not use glibc
```

Of course, all of this is customizable, be it per job, or simply by overriding the defaults above. I _tried_ to write it in a modular way.

### Test
Tests are done via `cargo test` (for test result integration in GitLab Merge Requests (MRs)) and [cargo-tarpaulin] for coverage information (also integrated in GitLab MRs).
`cargo-tarpaulin` does some internal magic so these are separate, but I would love to merge the two.

### Release Management
By default, all jobs related to release are disabled. They are conditioned by the following variables:
- `release:gitlab`: `$CI_COMMIT_TAG` exists and `$DEPLOY_GITLAB` is set to "yes".
- `release:crates`: `$CI_COMMIT_TAG` exists and `$DEPLOY_CRATES` is set to "yes". Please note you need to set `$CARGO_REGISTRY_TOKEN`.
- `pages`: We are on the repo's default branch or `$CI_COMMIT_TAG` exists and `$DEPLOY_PAGES` is set to "yes".

### GitLab Pages

This definition will attempt to deploy your docs on GitLab Pages, with the following structure:

```
public/
|- index.html # is a very simple menu offering links to the various versions available
|- latest/
|  |- index.html   # redirects to <crate_name>
|  -- crate_name/  # here lives your doc, as outputed by `cargo doc --no-deps`
|- v0.1.0/
|  |- index.html   # see above
|  -- crate_name/  # see above
[…]   
```


## Docker image

The provided docker image is based on `rust:stable`. Please have a look at the [Dockerfile](Dockerfile)!

## Contributions

Contributions are very welcome!

Here's how you can help:
- Raise issues, be it on bugs or undocumented behaviour;
- Propose features;
- Contribute code;
- Contribute documentation;
- …

## TODO

This is still heavily a work in progress, there's stuff that need doing.

- [x] Handle releases, including built binaries
- [ ] Better handle library releases.
- [x] Build our own Docker image, as to be able to run `cargo-tarpaulin` easily.
- [ ] Write better docs
- [ ] Add the ability to build Docker images
- [ ] Add the ability to release Docker images


[cargo clippy]: https://github.com/rust-lang/rust-clippy
[rustfmt]: https://github.com/rust-lang/rustfmt
[cargo-audit]: https://crates.io/crates/cargo-audit
[cargo-geiger]: https://crates.io/crates/cargo-geiger